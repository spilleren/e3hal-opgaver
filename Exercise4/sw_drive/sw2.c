#include <linux/gpio.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/module.h>

#define SW_MAJOR 64
#define SW_MINOR 0
#define SW_NBR_MINORS 8// 8-ch ADC
#define SW_GPIO 16 //

static int devno;
static struct cdev my_cdev;
struct file_operations my_fops;
int err = 0;
unsigned int no_devices = 1;
MODULE_LICENSE("GPL");
MODULE_AUTHOR("GRUPPE 18");
MODULE_DESCRIPTION("Chardriver til SW2");

int sw2_open(struct inode *inode, struct file *filep);
int sw2_release(struct inode *inode, struct file *filep);
ssize_t sw2_read(struct file *filep,char __user *buf, size_t count, loff_t *f_pos);

struct file_operations my_fops = {
  .open = sw2_open,
  .release = sw2_release,
  .read = sw2_read,
};


static int __init mysw_init(void)
{
  // Request GPIO
  err = gpio_request(SW_GPIO,"sw2");
  if(err < 0)
    goto err_reg;
  // Set GPIO direction (in or out)
  err = gpio_direction_input(SW_GPIO);
  if(err < 0)
    goto err_dir;
  // Make device no (vælg - Enten bruger I statisk eller også bruger I dynamisk major/minor nummer allokering)
  devno = MKDEV(SW_MAJOR,SW_MINOR);
  // Register Device
  err = register_chrdev_region(devno, SW_NBR_MINORS, "sw2");
  if (err < 0)
    goto err_free_buf;
// Cdev Init
  cdev_init(&my_cdev, &my_fops);

// Add Cdev
  err = cdev_add(&my_cdev, devno, no_devices);
  if (err <  0)
    goto err_dev_unregister;

  return 0; // success

  err_reg:
    // release gpio_register
    printk("Register failed!");
    gpio_free(SW_GPIO);

  err_dir:
    printk("Direction set failed!");
    gpio_free(SW_GPIO);

  err_dev_unregister:
    unregister_chrdev_region(devno,no_devices);
  err_free_buf:
    // Delete Cdev
    cdev_del(&my_cdev);
    // Unregister Device
    unregister_chrdev_region(devno,no_devices);
    // Free GPIO
    gpio_free(SW_GPIO);


return 0;
}

static void __exit mysw_exit(void)
 {
 // Delete Cdev
 cdev_del(&my_cdev);
 // Unregister Device
 unregister_chrdev_region(devno,no_devices);
 // Free GPIO
 gpio_free(SW_GPIO);
}

int sw2_open(struct inode *inode, struct file *filep)
{
   int major, minor;
   major = MAJOR(inode->i_rdev);
   minor = MINOR(inode->i_rdev);
   printk("Opening MyGpio Device [major], [minor]: %i, %i\n", major, minor);
  return 0;
}


int sw2_release(struct inode *inode, struct file *filep)
{
   int minor, major;

   major = MAJOR(inode->i_rdev);
   minor = MINOR(inode->i_rdev);
   printk("Closing/Releasing MyGpio Device [major], [minor]: %i, %i\n", major, minor);

  return 0;
}

ssize_t sw2_read(struct file *filep,char __user *buf, size_t count, loff_t *f_pos)
{
  char char_buf[2]; // buffer to hold the readvalue
  // read the value from SW_GPIO, and convert to char with sprintf
  sprintf(char_buf, "%d", gpio_get_value(SW_GPIO));
  // Check if the riden valuen is longer than count
  int buf_len = strlen(char_buf)+1;
  buf_len = buf_len > count ? count : buf_len;

  if (copy_to_user(buf,char_buf,buf_len)) {
    printk(KERN_ALERT "Error in: copy_to_user\n");
  }

  *f_pos += buf_len;
  return buf_len;
}

 module_init(mysw_init);
 module_exit(mysw_exit);
