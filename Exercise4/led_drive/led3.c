#include <linux/gpio.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/module.h>

#define LED_MAJOR 65
#define LED_MINOR 1
#define LED_NBR_MINORS 8// 8-ch ADC
#define LED_GPIO 21 //

static int devno;
static struct cdev my_cdev;
struct file_operations my_fops;
int err = 0;
unsigned int no_devices = 1;
MODULE_LICENSE("GPL");
MODULE_AUTHOR("GRUPPE 18");
MODULE_DESCRIPTION("Chardriver til LED3");

int led3_open(struct inode *inode, struct file *filep);
int led3_release(struct inode *inode, struct file *filep);
ssize_t led3_write(struct file *filep,const char __user *ubuf, size_t count, loff_t *f_pos);
ssize_t LED3_read(struct file *filep,char __user *buf, size_t count, loff_t *f_pos);



static int __init myled_init(void)
{
  // Request GPIO
  err = gpio_request(LED_GPIO,"led3");
  if(err < 0) // Error handling
    goto err_reg;
  // Set GPIO direction (in or out)
  err = gpio_direction_output(LED_GPIO,1);
  if(err < 0) // Error handling
    goto err_dir;
  // Make device no (static allocation)
  devno = MKDEV(LED_MAJOR,LED_MINOR);
  // Register Device
  err = register_chrdev_region(devno, LED_NBR_MINORS, "led3");
  if (err < 0) // Error handling
    goto err_free_buf;
// Cdev Init
  cdev_init(&my_cdev, &my_fops);

// Add Cdev
  err = cdev_add(&my_cdev, devno, no_devices);
  if (err <  0)
    goto err_dev_unregister;

  return 0; // success

  err_reg:
    // release gpio_register
    printk("Register failed!");
    gpio_free(LED_GPIO);

  err_dir:
    printk("Direction set failed!");
    gpio_free(LED_GPIO); // Release gpio_register

  err_dev_unregister:
    // Unregister device
    unregister_chrdev_region(devno,no_devices);
  err_free_buf:
    // Delete Cdev
    cdev_del(&my_cdev);
    // Unregister Device
    unregister_chrdev_region(devno,no_devices);
    // Free GPIO
    gpio_free(LED_GPIO);


return 0;
}

static void __exit myled_exit(void)
 {
 // Delete Cdev
 cdev_del(&my_cdev);
 // Unregister Device
 unregister_chrdev_region(devno,no_devices);
 // Free GPIO
 gpio_free(LED_GPIO);
}

int led3_open(struct inode *inode, struct file *filep)
{
   int major, minor;
   major = MAJOR(inode->i_rdev);
   minor = MINOR(inode->i_rdev);
   printk("Opening MyGpio Device [major], [minor]: %i, %i\n", major, minor);
  return 0;
}


int led3_release(struct inode *inode, struct file *filep)
{
   int minor, major;

   major = MAJOR(inode->i_rdev);
   minor = MINOR(inode->i_rdev);
   printk("Closing/Releasing MyGpio Device [major], [minor]: %i, %i\n", major, minor);

  return 0;
}

ssize_t led3_write(struct file *filep,const char __user *ubuf, size_t count, loff_t *f_pos)
{
  // minor = MINOR(filep->f_dentry->d_inode->i_rdev); Depricated!
  char char_buf[2]; //buffer to hold the write value as char
  int x;            //buffer to hold the write value as int
  //error handling, if the copy fails
  if (copy_from_user(char_buf, ubuf, count))
 {
   printk(KERN_ALERT "Error in: copy_from_user\n");
 };
   sscanf(char_buf, "%d", &x); // using sscanf to convert char to integer
   //use the gpio_set_value to change the value of LED_GPIO according to the written value
   gpio_set_value(LED_GPIO,x);
   *f_pos += count;
return count;
}

ssize_t LED3_read(struct file *filep,char __user *buf, size_t count, loff_t *f_pos)
{
  char char_buf[2]; /*
  sprintf(char_buf, "%d", gpio_get_value(LED_GPIO));
  int buf_len = strlen(char_buf)+1;
  buf_len = buf_len > count ? count : buf_len;

  if (copy_to_user(buf,char_buf,buf_len)) {
    printk(KERN_ALERT "Error in: copy_to_user\n");
  }

  *f_pos += buf_len;
  return buf_len;
}


 module_init(myled_init);
 module_exit(myled_exit);
