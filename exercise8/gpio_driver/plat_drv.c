#include <linux/gpio.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_gpio.h>
#include <linux/err.h>
#include <linux/timer.h>

#define GPIO_MINOR 0
#define NUMBER_OF_LEDS 3// 8-ch ADC

MODULE_LICENSE("GPL");
MODULE_AUTHOR("GRUPPE 18");
MODULE_DESCRIPTION("Chardriver til my_gpio");

static dev_t devno;
struct file_operations my_fops;
static int err = 0;
static int i;
unsigned int no_devices = 1;
static int gpio_devs_cnt = 0;

static struct cdev my_cdev;
static struct class *my_gpio_class;
static struct device *my_gpio_device;
static struct platform_driver my_gpio_plat_driver;

struct gpio_dev{
  uint8_t no; // GPIO number
  uint8_t dir; //0: in, 1: out
  uint8_t toggle_state; // Device attribute
  uint toggle_delay;
  struct timer_list ttimer;
};

static struct gpio_dev gpio_devs[255];
static int gpios_len = 3;

//timer callback function prototype
static void my_timer_callback(struct timer_list*);



// ***************Open Function*******************//
int my_gpio_open(struct inode *inode, struct file *filep)
{
   int major, minor;
   major = MAJOR(inode->i_rdev);
   minor = MINOR(inode->i_rdev);
   printk("Opening MyGpio Device [major], [minor]: %i, %i\n", major, minor);
  return 0;
}

// ***************Release Function*******************//
int my_gpio_release(struct inode *inode, struct file *filep)
{
   int minor, major;

   major = MAJOR(inode->i_rdev);
   minor = MINOR(inode->i_rdev);
   printk("Closing/Releasing MyGpio Device [major], [minor]: %i, %i\n", major, minor);

  return 0;
}

// ***************Read Function*******************//
ssize_t my_gpio_read(struct file *filep,char __user *buf, size_t count, loff_t *f_pos)
{
  char char_buf[2]; // buffer to hold the readvalue
  int minor;
  minor = iminor(filep->f_inode);
  // read the value from GPIO, and convert to char with sprintf
  sprintf(char_buf, "%d", gpio_get_value(gpio_devs[minor].no));
  // Check if the riden valuen is longer than count
  int buf_len = strlen(char_buf)+1;
  buf_len = buf_len > count ? count : buf_len;

  if (copy_to_user(buf,char_buf,buf_len)) {
    printk(KERN_ALERT "Error in: copy_to_user\n");
  }

  *f_pos += buf_len;
  return buf_len;
}
// ***************Write Function*******************//
ssize_t my_gpio_write(struct file *filep,const char __user *ubuf, size_t count, loff_t *f_pos)
{
  // minor = MINOR(filep->f_dentry->d_inode->i_rdev); Depricated!
  char char_buf[2]; //buffer to hold the write value as char
  int x;            //buffer to hold the write value as int
  int minor;
  minor = iminor(filep->f_inode);
  //error handling, if the copy fails
  if (copy_from_user(char_buf, ubuf, count))
 {
   printk(KERN_ALERT "Error in: copy_from_user\n");
 };
   sscanf(char_buf, "%d", &x); // using sscanf to convert char to integer
   //use the gpio_set_value to change the value of LED_GPIO according to the written value
   gpio_set_value(gpio_devs[minor].no,x);
   *f_pos += count;
return count;
}


struct file_operations my_fops = {
  .owner = THIS_MODULE,
  .open = my_gpio_open,
  .read = my_gpio_read,
  .write = my_gpio_write,
  .release = my_gpio_release,
};


//***************Probe Function *****************//
static int my_probe(struct platform_device* pdev)
{
  printk(KERN_ALERT"Hello there! (From probe)\n");
  int err = 0;
  struct device *dev = &pdev ->dev; //device ptr derived from current platform_device
  struct device_node *np = dev->of_node; // Device tree node ptr
  enum of_gpio_flags flag;
  int gpios_in_dt = 0;
  char buf[20];

// Retrieve number of gpios from node pointer
  if ((gpios_in_dt = of_gpio_count(np)) < 0){
    dev_err(&pdev->dev, "Failed to retrieve number of gpios\n");
    return -EINVAL;
  } else if (gpios_in_dt == 0){
    printk("gpios count equals 0\n");
  }

  printk(KERN_ALERT "Number of gpios: %d\n",gpios_in_dt);


  // Loop through device tree and get gpio_number and Direction
  for (i = 0; i < gpios_in_dt; i++) {
    gpio_devs[i].no = of_get_gpio(np,i);
    if(gpio_devs[i].no < 0){
      if(gpio_devs[i].no != -EPROBE_DEFER){
        dev_err(my_gpio_device,"Failed to parse gpio %d\n", gpio_devs[i].no);
      }
    }
    err = of_get_gpio_flags(np,i,&flag);
    gpio_devs[i].dir = flag;
  }

  //Request GPIO, set direction and create class with forloop

  for (i = 0; i < gpios_len; i++) {
   //Device Create
   sprintf(buf,"gpio%i",i);
   err = gpio_request(gpio_devs[i].no, buf);
   if(err != 0)
     goto err_reg;

   // Set GPIO direction (in or out)
   if (gpio_devs[i].dir == 0) //Input
   {
     err = gpio_direction_input(gpio_devs[i].no);
     if(err != 0)
       goto err_direction;
   }
   else { //Output
     err = gpio_direction_output(gpio_devs[i].no, 0);
     if(err != 0)
       goto err_direction;
   }

   // Create device with major and minor, pass gpio_devs as device data
   my_gpio_device = device_create(my_gpio_class, NULL, MKDEV(MAJOR(devno),gpio_devs_cnt),
                                  &gpio_devs[i], "mydevice%d", gpio_devs[i].no);
   // Settings default values
   gpio_devs[i].toggle_state = 0;
   gpio_devs[i].toggle_delay = 1000;
   // Setup timer for each individual device
   timer_setup(&gpio_devs[i].ttimer,my_timer_callback, 0);



   if(IS_ERR(my_gpio_device)){
     printk(KERN_ALERT "Failed to create device\n");
     return -EFAULT;
   }
   else{
    printk(KERN_ALERT "Created device /dev/mydevice%d with major:%d and minor: %d\n",gpio_devs[i].no, MAJOR(devno),gpio_devs_cnt);
    if(gpio_devs_cnt < (gpios_in_dt-1))
      gpio_devs_cnt++;
    }


 }
 return 0; // No errors encountered

 //Error Handling
 err_direction:
   printk("Direction set failed!\n");
   gpio_free(gpio_devs[i].no);
 err_reg:
   if(i > 0){
       gpio_free(gpio_devs[i].no);
       printk("Register failed!\n");
   }
   if(i == 0);
   printk("Failed to setup GPIO%d\n",gpio_devs[i].no);

   return err;
}
//************Probe Remove Function *******************//
static int my_remove(struct platform_device* pdev)
{
 //GPIO Request & direction remove
 printk(KERN_ALERT "GENERAL KENOBI!! (From remove)\n");
 //Free GPIO & Destroy Class
 for (i = 0; i < gpios_len; i++) {
   gpio_free(gpio_devs[i].no);
   //Destroy class
   device_destroy(my_gpio_class, MKDEV(MAJOR(devno),i));
   printk(KERN_ALERT "Device: %i destroyed\n",i);
 }
 return 0;
}

static const struct of_device_id my_gpio_plat_driver_match[] =
{
   { .compatible = "ase, plat_drv",}, {},
};

static struct platform_driver my_gpio_plat_driver =
{
   .probe = my_probe,
   .remove = my_remove,
   .driver = {
               .name = "my_sw_old_naming",
               .of_match_table = my_gpio_plat_driver_match,
               .owner = THIS_MODULE,
             },
};
static void my_timer_callback(struct timer_list *t){
  // Derive device from associated timer
  struct gpio_dev *this_dev = from_timer(this_dev,t,ttimer);

  // Get GPIO and current value
  uint8_t this_gpio = this_dev->no;
  int value = gpio_get_value(this_gpio);

  printk("Toggling gpio%d with value %d\n",this_gpio, value);

  // Toggle value to the new value with gpio_set_value
  int tmp_value = (value > 0 ? 0 : 1);
  gpio_set_value(this_gpio,tmp_value);

  mod_timer(&this_dev->ttimer,jiffies + msecs_to_jiffies(this_dev->toggle_delay));

}


// ************* Show + store functions for gpio_toggle_state*****
static ssize_t led_toggle_state_show(struct device *dev, struct device_attribute *attr, char *buf){
  // Get device toggle_state with dev_get_drvdata
  struct gpio_dev* this_dev = dev_get_drvdata(dev);
  printk("Device toggle_state is: %d\n", this_dev->toggle_state);
  return 0;
}

static ssize_t led_toggle_state_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t size){
  ssize_t ret = -EINVAL;
  unsigned long value;

  if((ret = kstrtoul(buf,10,&value))<0)
    return ret;

  struct gpio_dev* this_dev = dev_get_drvdata(dev);

  /* Do something with value */
  this_dev->toggle_state = value;
  ret = size; // Always read the full content of buf
  return ret;
}

static ssize_t led_toggle_delay_show(struct device *dev, struct device_attribute *attr, char *buf){
  // Get device toggle_state with dev_get_drvdata
  struct gpio_dev* this_dev = dev_get_drvdata(dev);
  printk("Device toggle_delay is: %d\n", this_dev->toggle_delay);
  return 0;
}

static ssize_t led_toggle_delay_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t size){
  ssize_t ret = -EINVAL;
  unsigned long value;

  if((ret = kstrtoul(buf,10,&value))<0)
    return ret;

  struct gpio_dev* this_dev = dev_get_drvdata(dev);

  /* Do something with value */
  if(value == 0 || value == 1){
    if(value == 1){
      //Start timer
      printk("Starting timer on gpio%d\n",this_dev->no);
      mod_timer(&this_dev->ttimer, jiffies + msecs_to_jiffies(this_dev->toggle_delay));
    } else{
      //Stop timer
      printk("Stopping timer on gpio%d\n",this_dev->no);
      del_timer(&this_dev->ttimer);
    }
  }
  this_dev->toggle_delay = value;
  ret = size; // Always read the full content of buf
  return ret;
}

DEVICE_ATTR_RW(led_toggle_state); // Creates dev_attr_led_toggle_state
DEVICE_ATTR_RW(led_toggle_delay); // Creates dev_attr_led_toggle_delay

static struct attribute *led_attrs[] = { // Create using the macro instead of many functions
  &dev_attr_led_toggle_state.attr,
  &dev_attr_led_toggle_delay.attr,
  NULL,
};

ATTRIBUTE_GROUPS(led); //Creates led_groups

// ***************Init Function*******************//
static int mysw_init(void)
{
  printk("Initialize LED driver\n");
  // Register Device
  err = alloc_chrdev_region(&devno, GPIO_MINOR, NUMBER_OF_LEDS, "my_gpio");
  if(err < 0){
    printk("Failed to allocate device major\n");
    goto err_alloc;
  }else if(MAJOR(devno) <= 0){
    printk("Major number zero or less\n");
    goto err_class;
  }
  printk(KERN_ALERT "Assigned major number: %i\n", MAJOR(devno));

// Class Create
  my_gpio_class = class_create(THIS_MODULE, "my_gpio_class");
  if (IS_ERR(my_gpio_class)) {
    printk(KERN_ALERT"ERROR, failed to create class. \n");
    goto err_class;
  }
  my_gpio_class->dev_groups = led_groups;

  //Platform register
  err = platform_driver_register(&my_gpio_plat_driver);
  if(err < 0){
    printk("Failed register platform driver\n");
    goto err_register;
  }

  //Char device init
  cdev_init(&my_cdev, &my_fops);
  //Register char device
  err = cdev_add(&my_cdev, devno, NUMBER_OF_LEDS);
  if(err < 0){
    goto err_platform;
  }

  printk(KERN_ALERT "\nInitialization of LED module was successful.\n");

  return 0; // success
  /**** ERROR HANDLING ****/
  // Unreg platform driver
  err_platform:
    platform_driver_unregister(&my_gpio_plat_driver);
  err_register:
    //destroy class
    class_destroy(my_gpio_class);
  // Unreg device numbers
  err_class:
    unregister_chrdev_region(devno, NUMBER_OF_LEDS);
  err_alloc:
    return err;
}

// ***************Exit Function*******************//
static void mysw_exit(void)
 {
   printk(KERN_ALERT" Exit function called\n");
//Removing platform_device
 platform_driver_unregister(&my_gpio_plat_driver);
//Class Destroy
 class_destroy(my_gpio_class);
 // Delete Cdev
 cdev_del(&my_cdev);
 // Unregister Device
 unregister_chrdev_region(devno,no_devices);


 printk(KERN_ALERT "Exit function complete.\n");
 return;
}

// Init & Exit
module_init(mysw_init);
module_exit(mysw_exit);
