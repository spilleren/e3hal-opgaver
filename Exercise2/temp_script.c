#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <i2c/smbus.h>

void initLED();
int main()

{
  char on [] = "1";
  char off [] = "0";
  char buffer[1];
  char temp[2];
  char webprint[128];

  int fd, fd1, fd2;

  initLED();
  // assign fd to the gpio26 which we open and set as read&write
//  fd= open("/sys/class/gpio/gpio26/value", O_RDWR);
  fd = open("/dev/i2c-1", O_RDONLY);
  fd1 = open("/sys/class/gpio/gpio26/value", O_RDWR);
  fd2 = open("/www/pages/index.html",O_WRONLY);
  ioctl(fd, 0x0703, 0x48); // i2cdev sys call (0x0703) to set I2C addr


  while(1)
    {'
      //Read data from I2C and save in temp
      read(fd, temp, 2);
      printf("Temp %d \n", temp[0]);
      // Save a string in webprint
      sprintf(webprint, "<html><body><h1> Temperaturen: %d,%d\n </h1></body></html>", temp[0], (temp[1] == 128 ? 5 : 0));
      // copy the webprint to the webpage
      write(fd2,webprint,strlen(webprint));

      //chech if the temperature is above or equal to 31 degrees
      if (temp[0] >= 31) {

        //Tell the user it is to hot
        printf("WoW det er alt for varmt! Pas på!\n");
        //Turn on the LED
        write(fd1,on,1);
      }
      else
      //Turn off the LED
      write(fd1,off,1);

      sleep(1);
    }
    close(fd);
    close(fd1);
}


void initLED(){

    char gpio[1] = {26};
    char direction[3] = "out";

    int fd1 = open("/sys/class/gpio/export", O_WRONLY);

    write(fd1, gpio, 1);

    close(fd1);

    int fd2 = open("/sys/class/gpio/gpio26/direction", O_WRONLY);

    write(fd2, direction, 3);

    close(fd2);
}
