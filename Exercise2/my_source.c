#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

int main()

{
  char on [] = "1";
  char off [] = "0";
  int fd;


// assign fd to the gpio26 which we open and set as read&write
fd= open("/sys/class/gpio/gpio26/value", O_RDWR); //


 while(1)
   {
     //write on (1) to the LED
     write(fd, on, 1);
     //wait 1 second
     sleep(1);
     //write off (0) to the LED
     write(fd, off, 1);
     //wait 1 second
     sleep(1);
   }
//close the file
close(fd);
}
