#include <linux/gpio.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/sched.h>

#define SW_MINOR 0
#define SW_NBR_MINORS 8// 8-ch ADC
#define SW_GPIO 16//

static DECLARE_WAIT_QUEUE_HEAD(wq);
static int flag = 0;
static int devno;
struct inode;
static struct cdev my_cdev;
struct file_operations my_fops;
static int err = 0;
static int isr_gpio_value = 0;
static int proc_gpio_value = 0;


MODULE_LICENSE("GPL");
MODULE_AUTHOR("GRUPPE 18");
MODULE_DESCRIPTION("Chardriver til SW2");

int sw2_open(struct inode *inode, struct file *filep);
int sw2_release(struct inode *inode, struct file *filep);
ssize_t sw2_read(struct file *filep,char __user *buf, size_t count, loff_t *f_pos);

struct file_operations my_fops = {
  .owner = THIS_MODULE,
  .open = sw2_open,
  .read = sw2_read,
  .release = sw2_release,
};

static irqreturn_t mysw_isr(int irq, void *dev_id){
  flag = 1;
  wake_up_interruptible(&wq);
  isr_gpio_value = gpio_get_value(SW_GPIO);
  return IRQ_HANDLED;
};

static int __init mysw_init(void)
{
  // Request GPIO
  err = gpio_request(SW_GPIO,"sw2");
  if(err < 0)
    goto err_reg;
  // Set GPIO direction (in or out)
  err = gpio_direction_input(SW_GPIO);
  if(err < 0)
    goto err_free;

  // Register Device
  err = alloc_chrdev_region(&devno, SW_MINOR, SW_NBR_MINORS, "sw2");
  if (err < 0)
    goto err_free;

// Cdev Init
  cdev_init(&my_cdev, &my_fops);

// Add Cdev
  err = cdev_add(&my_cdev, devno, SW_NBR_MINORS);
  if (err <  0)
    goto err_dev_unregister;

// Attach irq line to GPIO port


  err = request_irq(gpio_to_irq(SW_GPIO), mysw_isr, IRQF_TRIGGER_RISING, "sw_irq",NULL);
  if (err < 0)
    goto err_dev_unregister;

  printk(KERN_ALERT "\nInitialization of SW module was successful.\n");

  return 0; // success

  err_dev_unregister:
    unregister_chrdev_region(devno, SW_NBR_MINORS);
  err_free:
    printk("Direction set failed or Device Register failed!");
  err_reg:
    // release gpio_register
    printk("Register failed!");
    gpio_free(SW_GPIO);
    return err;
}

static void __exit mysw_exit(void)
 {
 // Delete Cdev
 cdev_del(&my_cdev);
 // Unregister Device
 unregister_chrdev_region(devno, SW_NBR_MINORS);
 // Free IRQ
 free_irq(gpio_to_irq(SW_GPIO),NULL);
 // Free GPIO
 gpio_free(SW_GPIO);

 printk(KERN_ALERT "Exit function complete.\n");
 return;
}

int sw2_open(struct inode *inode, struct file *filep)
{
   int major, minor;
   major = MAJOR(inode->i_rdev);
   minor = MINOR(inode->i_rdev);
   printk("Opening MyGpio Device [major], [minor]: %i, %i\n", major, minor);
  return 0;
}


int sw2_release(struct inode *inode, struct file *filep)
{
   int minor, major;

   major = MAJOR(inode->i_rdev);
   minor = MINOR(inode->i_rdev);
   printk("Closing/Releasing MyGpio Device [major], [minor]: %i, %i\n", major, minor);

  return 0;
}

ssize_t sw2_read(struct file *filep,char __user *buf, size_t count, loff_t *f_pos)
{
  wait_event_interruptible(wq,flag == 1); //Wait for event trigger
  proc_gpio_value = gpio_get_value(SW_GPIO); // get value from SW_GPIO
  char char_buf[10]; // buffer to hold the readvalue
  // read the value from SW_GPIO, and convert to char with sprintf
  sprintf(char_buf, "%d %d\n", proc_gpio_value, isr_gpio_value);
  // Check if the riden valuen is longer than count
  int buf_len = strlen(char_buf)+1;
  buf_len = buf_len > count ? count : buf_len;

  flag = 0;

  if (copy_to_user(buf,char_buf,buf_len)) {
    printk(KERN_ALERT "Error in: copy_to_user\n");
  }

  *f_pos += buf_len;

  return buf_len;
}

 module_init(mysw_init);
 module_exit(mysw_exit);
