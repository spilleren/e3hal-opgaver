#include <linux/gpio.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_gpio.h>
#include <linux/err.h>

#define GPIO_MINOR 0
#define GPIO_NBR_MINORS 8// 8-ch ADC
#define GPIO 16 //

MODULE_LICENSE("GPL");
MODULE_AUTHOR("GRUPPE 18");
MODULE_DESCRIPTION("Chardriver til my_gpio");

static dev_t devno;
struct file_operations my_fops;
static int err = 0;
static int i;
unsigned int no_devices = 1;
static int gpio_devs_cnt = 0;

static struct cdev my_cdev;
static struct class *my_gpio_class;
static struct device *my_gpio_device;
static struct platform_driver my_gpio_plat_driver;

struct gpio_dev{
  int no; // GPIO number
  int dir; //0: in, 1: out
};

static struct gpio_dev gpio_devs[255];
static int gpios_len = 2;


// ***************Exit Function*******************//
static int __init mysw_init(void)
{
  // Register Device
  alloc_chrdev_region(&devno, GPIO_MINOR, GPIO_NBR_MINORS, "my_gpio");


// Cdev Init
  cdev_init(&my_cdev, &my_fops);
// Add Cdev
  err = cdev_add(&my_cdev, devno, GPIO_NBR_MINORS);
  if (err <  0)
    goto err_dev_unregister;
// Class Create
  my_gpio_class = class_create(THIS_MODULE, "my_gpio_class");
  if (IS_ERR(my_gpio_class)) {
    printk(KERN_ALERT"ERROR, failed to create class. \n");
    goto classCreateFail;
  }

//Platform register
  platform_driver_register(&my_gpio_plat_driver);

  printk(KERN_ALERT "\nInitialization of LED module was successful.\n");

  return 0; // success

  classCreateFail:
    class_destroy(my_gpio_class);

  err_dev_unregister:
    unregister_chrdev_region(devno, GPIO_NBR_MINORS);

  return 0;
}

// ***************Exit Function*******************//
static void __exit mysw_exit(void)
 {
   printk(KERN_ALERT" Exit function called");
//Removing platform_device
 platform_driver_unregister(&my_gpio_plat_driver);
//Class Destroy
 class_destroy(my_gpio_class);
 // Delete Cdev
 cdev_del(&my_cdev);
 // Unregister Device
 unregister_chrdev_region(devno,no_devices);


 printk(KERN_ALERT "Exit function complete.\n");
 return;
}
// ***************Open Function*******************//
int my_gpio_open(struct inode *inode, struct file *filep)
{
   int major, minor;
   major = MAJOR(inode->i_rdev);
   minor = MINOR(inode->i_rdev);
   printk("Opening MyGpio Device [major], [minor]: %i, %i\n", major, minor);
  return 0;
}

// ***************Release Function*******************//
int my_gpio_release(struct inode *inode, struct file *filep)
{
   int minor, major;

   major = MAJOR(inode->i_rdev);
   minor = MINOR(inode->i_rdev);
   printk("Closing/Releasing MyGpio Device [major], [minor]: %i, %i\n", major, minor);

  return 0;
}

// ***************Read Function*******************//
ssize_t my_gpio_read(struct file *filep,char __user *buf, size_t count, loff_t *f_pos)
{
  char char_buf[2]; // buffer to hold the readvalue
  int minor;
  minor = iminor(filep->f_inode);
  // read the value from GPIO, and convert to char with sprintf
  sprintf(char_buf, "%d", gpio_get_value(gpio_devs[minor].no));
  // Check if the riden valuen is longer than count
  int buf_len = strlen(char_buf)+1;
  buf_len = buf_len > count ? count : buf_len;

  if (copy_to_user(buf,char_buf,buf_len)) {
    printk(KERN_ALERT "Error in: copy_to_user\n");
  }

  *f_pos += buf_len;
  return buf_len;
}
// ***************Write Function*******************//
ssize_t my_gpio_write(struct file *filep,const char __user *ubuf, size_t count, loff_t *f_pos)
{
  // minor = MINOR(filep->f_dentry->d_inode->i_rdev); Depricated!
  char char_buf[2]; //buffer to hold the write value as char
  int x;            //buffer to hold the write value as int
  int minor;
  minor = iminor(filep->f_inode);
  //error handling, if the copy fails
  if (copy_from_user(char_buf, ubuf, count))
 {
   printk(KERN_ALERT "Error in: copy_from_user\n");
 };
   sscanf(char_buf, "%d", &x); // using sscanf to convert char to integer
   //use the gpio_set_value to change the value of LED_GPIO according to the written value
   gpio_set_value(gpio_devs[minor].no,x);
   *f_pos += count;
return count;
}
// Init & Exit
module_init(mysw_init);
module_exit(mysw_exit);

struct file_operations my_fops = {
  .owner = THIS_MODULE,
  .open = my_gpio_open,
  .read = my_gpio_read,
  .write = my_gpio_write,
  .release = my_gpio_release,
};


//***************Probe Function *****************//
static int my_probe(struct platform_device* pdev)
{
  printk(KERN_ALERT"Hello there! (From probe)\n");
  int err = 0;
  struct device *dev = &pdev ->dev; //device ptr derived from current platform_device
  struct device_node *np = dev->of_node; // Device tree node ptr
  enum of_gpio_flags flag;
  int gpios_in_dt = 0;
  char buf[20];

// Retrieve number of gpios from node pointer
  if ((gpios_in_dt = of_gpio_count(np)) < 0){
    dev_err(&pdev->dev, "Failed to retrieve number of gpios\n");
    return -EINVAL;
  } else if (gpios_in_dt == 0){
    printk("gpios count equals 0\n");
  }

  printk(KERN_ALERT "Number of gpios: %d\n",gpios_in_dt);


  // Loop through device tree and get gpio_number and Direction
  for (i = 0; i < gpios_in_dt; i++) {
    gpio_devs[i].no = of_get_gpio(np,i);
    if(gpio_devs[i].no < 0){
      if(gpio_devs[i].no != -EPROBE_DEFER){
        dev_err(my_gpio_device,"Failed to parse gpio %d\n", gpio_devs[i].no);
      }
    }
    err = of_get_gpio_flags(np,i,&flag);
    gpio_devs[i].dir = flag;
  }

  //Request GPIO, set direction and create class with forloop

  for (i = 0; i < gpios_len; i++) {
   //Device Create
   sprintf(buf,"gpio%i",i);
   err = gpio_request(gpio_devs[i].no, buf);
   if(err != 0)
     goto err_reg;

   // Set GPIO direction (in or out)
   if (gpio_devs[i].dir == 0) //Input
   {
     err = gpio_direction_input(gpio_devs[i].no);
     if(err != 0)
       goto err_direction;
   }
   else { //Output
     err = gpio_direction_output(gpio_devs[i].no, 0);
     if(err != 0)
       goto err_direction;
   }

   // Create device with major and minor
   my_gpio_device = device_create(my_gpio_class, NULL, MKDEV(MAJOR(devno),gpio_devs_cnt),
                                  NULL, "mydevice%d", i);
   if(IS_ERR(my_gpio_device)){
     printk(KERN_ALERT "Failed to create device\n");
     return -EFAULT;
   }
   else
    printk(KERN_ALERT "Created device /dev/mydevice%d with major:%d and minor: %d\n",gpio_devs[i].no, MAJOR(devno),gpio_devs_cnt);
    if(gpio_devs_cnt < (gpios_in_dt-1))
      gpio_devs_cnt++;


 }
 return 0; // No errors encountered

 //Error Handling
 err_direction:
   printk("Direction set failed!");
   gpio_free(gpio_devs[i].no);
 err_reg:
   if(i > 0){
       gpio_free(gpio_devs[i].no);
       printk("Register failed!");
   }
   if(i == 0);
   printk("Failed to setup GPIO%d\n",gpio_devs[i].no);

   return err;
}
//************Probe Remove Function *******************//
static int my_remove(struct platform_device* pdev)
{
 //GPIO Request & direction remove
 printk(KERN_ALERT "GENERAL KENOBI!! (From remove)\n");
 //Free GPIO & Destroy Class
 for (i = 0; i < gpios_len; i++) {
   gpio_free(gpio_devs[i].no);
   //Destroy class
   device_destroy(my_gpio_class, MKDEV(MAJOR(devno),i));
   printk(KERN_ALERT "Device: %i destroyed",i);
 }
 return 0;
}

static const struct of_device_id my_gpio_plat_driver_match[] =
{
   { .compatible = "ase, plat_drv",}, {},
};

static struct platform_driver my_gpio_plat_driver =
{
   .probe = my_probe,
   .remove = my_remove,
   .driver = {
               .name = "my_sw_old_naming",
               .of_match_table = my_gpio_plat_driver_match,
               .owner = THIS_MODULE,
             },
};
